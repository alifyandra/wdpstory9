# Story 9
This Gitlab repository is the result of work from **Ahmad Izzudin Alifyandra**

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/alifyandra/wdpstory9/badges/master/pipeline.svg)](https://gitlab.com/alifyandra/wdpstory9/commits/master)
[![coverage report](https://gitlab.com/alifyandra/wdpstory9/badges/master/coverage.svg)](https://gitlab.com/alifyandra/wdpstory9/commits/master)

## URL
This lab projects can be accessed from [https://bookapi9.herokuapp.com](https://bookapi9.herokuapp.com)

## Author
**Ahmad Izzudin Alifyandra** - [alifyandra](https://gitlab.com/alifyandra)