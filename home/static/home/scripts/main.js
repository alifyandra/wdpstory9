var likes;
var likehtml;
var response;
var isSearched = false;
var search;
$(document).ready(function () {
    $("#searchForm").submit(function () {
        search = $("#searchInput").val();
        if (search == "") {
            alert("Please enter something in the field first");
        } else {
            $("#result").text("");
            $("#modbtnlabel").text("Top 5 '" + search + "' books");
            isSearched = true;
            likes = [
                [0, 0],
                [0, 1],
                [0, 2],
                [0, 3],
                [0, 4],
                [0, 5],
                [0, 6],
                [0, 7],
                [0, 8],
                [0, 9],
            ];
            likehtml = "";
            var img = "";
            var title = "";
            var author = "";
            var description = "";
            var publisher = "";
            var publishdate = "";
            var url = "";
            var starttable =
                '<table class="table table-borderless table-dark" style="border-radius: 15px;">' +
                "<thead>" +
                "<tr>" +
                '<th scope="col">#</th>' +
                '<th scope="col">Likes</th>' +
                '<th scope="col">Image</th>' +
                '<th scope="col">Title</th>' +
                '<th scope="col">Author</th>' +
                '<th scope="col">Publisher</th>' +
                '<th scope="col">Publish Date</th>' +
                '<th scope="col">URL</th>' +
                "</tr>" +
                "</thead>" +
                "<tbody>";
            var endtable = "</tbody> </table>";
            var html = "";

            $.get(
                "https://www.googleapis.com/books/v1/volumes?q=" + search,
                function (response) {
                    console.log(response);
                    if (!response.totalItems) {
                        $("#result").html(
                            "<h2>'" + search + "' not found.</h2>"
                        );
                    }
                    html += starttable;
                    for (
                        i = 0;
                        i < response.items.length;
                        i++ //items is name of array
                    ) {
                        //like button and counter
                        likehtml =
                            '<a onclick="like(' +
                            i +
                            ')"><i class="fas fa-heart" style="color:#eb4848;"></i></a>' +
                            '<span id="like' +
                            i +
                            '"> 0</span>';
                        //get title of book
                        title = response.items[i].volumeInfo.title;
                        //get author
                        author = response.items[i].volumeInfo.authors;
                        //get desc
                        description = response.items[i].volumeInfo.description;
                        //get publisher
                        publisher = response.items[i].volumeInfo.publisher;
                        //get pubdate
                        publishdate =
                            response.items[i].volumeInfo.publishedDate;
                        //get book img
                        if (
                            response.items[i].volumeInfo.imageLinks == undefined
                        ) {
                            img = "no image";
                        } else {
                            imgurl =
                                response.items[i].volumeInfo.imageLinks
                                    .thumbnail;
                            img =
                                '<img src="' +
                                imgurl +
                                '"width="60" height="80">';
                        }
                        //get book url
                        url =
                            '<a href="' +
                            response.items[i].volumeInfo.infoLink +
                            '"><button class="btn btn-success">Read</button></a>';
                        row =
                            '<tr id="row' +
                            i +
                            '"> <th scope="row">' +
                            (i + 1) +
                            "</th> " +
                            "<td>" +
                            likehtml +
                            "</td> " +
                            '<td id="img' +
                            i +
                            '">' +
                            img +
                            "</td> " +
                            '<td id="title' +
                            i +
                            '">' +
                            title +
                            "</td> " +
                            "<td>" +
                            author +
                            "</td> " +
                            "<td>" +
                            publisher +
                            "</td> " +
                            "<td>" +
                            publishdate +
                            "</td> " +
                            '<td id="url' +
                            i +
                            '">' +
                            url +
                            "</td> " +
                            "</tr>";
                        html += row;
                    }
                    html += endtable;
                    $("#result").append(html);
                }
            );
        }
        return false;
    });
});
function like(i) {
    likes[i][0]++;
    $("#like" + i).text(" " + likes[i][0]);
    console.log("like" + i + "=" + likes[i][0]);
}
function cmp(a, b) {
    return b[0] - a[0];
}
function modal() {
    if (isSearched) {
        $("#modalbody").text("");
        var likesM = likes.slice();
        likesM.sort(cmp);
        console.log(likesM);
        console.log(likes);
        var modalhtml =
            '<div class="table-responsive">' +
            '<table class="table table-borderless" style="border-radius: 15px;">' +
            "<thead>" +
            "<tr>" +
            '<th scope="col">Likes</th>' +
            '<th scope="col">Image</th>' +
            '<th scope="col">Title</th>' +
            '<th scope="col">URL</th>' +
            "</tr>" +
            "</thead>" +
            "<tbody>";
        for (i = 0; i < 5; i++) {
            modalhtml += "<tr>";
            modalhtml +=
                "<td>" +
                likesM[i][0] +
                "</td> " +
                "<td>" +
                $("#img" + likesM[i][1]).html() +
                "</td> " +
                "<td>" +
                $("#title" + likesM[i][1]).text() +
                "</td> " +
                "<td>" +
                $("#url" + likesM[i][1]).html() +
                "</td> ";
            modalhtml += "</tr>";
        }
        modalhtml += "</tbody> </table> </div>";
        $("#modalbody").append(modalhtml);
    }
}
